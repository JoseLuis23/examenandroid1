package com.example.examenandroid1;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class CuentaBancoActivity extends AppCompatActivity {

    private EditText numeroCuenta;
    private EditText nombre;
    private EditText banco;
    private EditText saldo;
    private Spinner spinnerMovimientos;
    private EditText cantidad;
    private TextView nuevoSaldo;
    private Button btnAplicar;
    private Button btnRegresar;
    private Button btnLimpiar;

    private CuentaBanco cuentaBanco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);

        numeroCuenta = findViewById(R.id.numeroCuenta);
        nombre = findViewById(R.id.nombre);
        banco = findViewById(R.id.banco);
        saldo = findViewById(R.id.saldo);
        spinnerMovimientos = findViewById(R.id.spinnerMovimientos);
        cantidad = findViewById(R.id.cantidad);
        nuevoSaldo = findViewById(R.id.nuevoSaldo);
        btnAplicar = findViewById(R.id.btnAplicar);
        btnRegresar = findViewById(R.id.btnRegresar);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        // Configurar el Spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.movimientos_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMovimientos.setAdapter(adapter);

        btnAplicar.setOnClickListener(v -> {
            if (cuentaBanco == null || isAccountFieldsChanged()) {
                cuentaBanco = new CuentaBanco(
                        numeroCuenta.getText().toString(),
                        nombre.getText().toString(),
                        banco.getText().toString(),
                        Float.parseFloat(saldo.getText().toString())
                );
            }

            String tipoMovimiento = spinnerMovimientos.getSelectedItem().toString();
            float cantidadMovimiento = Float.parseFloat(cantidad.getText().toString());

            if (tipoMovimiento.equals("Depositar")) {
                cuentaBanco.depositar(cantidadMovimiento);
                nuevoSaldo.setText(String.valueOf(cuentaBanco.getSaldo()));
            } else if (tipoMovimiento.equals("Retirar")) {
                if (cuentaBanco.getSaldo() >= cantidadMovimiento) {
                    cuentaBanco.retirar(cantidadMovimiento);
                    nuevoSaldo.setText(String.valueOf(cuentaBanco.getSaldo()));
                } else {
                    Toast.makeText(this, "No cuenta con el saldo suficiente", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnRegresar.setOnClickListener(v -> finish());

        btnLimpiar.setOnClickListener(v -> {
            numeroCuenta.setText("");
            nombre.setText("");
            banco.setText("");
            saldo.setText("");
            cantidad.setText("");
            nuevoSaldo.setText("");
            cuentaBanco = null;
        });
    }

    private boolean isAccountFieldsChanged() {
        return !numeroCuenta.getText().toString().equals(cuentaBanco.getNumeroCuenta()) ||
                !nombre.getText().toString().equals(cuentaBanco.getNombre()) ||
                !banco.getText().toString().equals(cuentaBanco.getBanco()) ||
                Float.parseFloat(saldo.getText().toString()) != cuentaBanco.getSaldo();
    }
}
