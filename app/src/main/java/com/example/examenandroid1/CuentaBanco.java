package com.example.examenandroid1;


public class CuentaBanco {
    private String numeroCuenta;
    private String nombre;
    private String banco;
    private float saldo;

    public CuentaBanco(String numeroCuenta, String nombre, String banco, float saldo) {
        this.numeroCuenta = numeroCuenta;
        this.nombre = nombre;
        this.banco = banco;
        this.saldo = saldo;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public void depositar(float cantidad) {
        this.saldo += cantidad;
    }

    public void retirar(float cantidad) {
        if (this.saldo >= cantidad) {
            this.saldo -= cantidad;
        } else {
            System.out.println("Saldo insuficiente");
        }
    }
}



